from django.db import models
from .validator import validate_file_extension

# Create your models here.
class Melamar(models.Model):
    nama_pelamar = models.CharField(max_length=50, null=True)
    pesan = models.TextField(null=True)
    kontak_pelamar = models.TextField(null=True)
    cv_pelamar = models.TextField(null=True, validators=[validate_file_extension])
    id_project = models.IntegerField(default=0, null=True, blank=True)
    id_user = models.TextField(null=True, blank=True)