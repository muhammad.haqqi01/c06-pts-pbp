from django.apps import AppConfig


class MelamarConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'melamar'
