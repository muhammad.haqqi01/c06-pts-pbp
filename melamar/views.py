from django.http import JsonResponse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from melamar.models import Melamar
from melamar.forms import MelamarForm
from django.contrib.auth.decorators import login_required
import json
from django.views.decorators.csrf import csrf_exempt
from signup.models import User

@login_required(login_url="/login/")
def add_lamar(request, id):
    if request.method == 'POST':
        nama_pelamar = request.POST.get('nama_pelamar')
        pesan = request.POST.get('pesan')
        kontak_pelamar = request.POST.get('kontak_pelamar')
        cv_pelamar = request.POST.get('cv_pelamar')
        id_project = request.POST.get('id_project')

        new_project = Melamar(
            nama_pelamar=nama_pelamar,
            pesan=pesan,
            kontak_pelamar=kontak_pelamar,
            cv_pelamar=cv_pelamar,
            id_project=id_project,
            id_user = request.user.username,
        )

        new_project.save()
        
        return JsonResponse({"instance": "Lamaran Diterima"}, status=200)

    return render(request, 'melamar.html')

@csrf_exempt
def connect_melamar(request, id):
    if request.method == 'POST':
        newLamar = json.loads(request.body)

        new_lamar = Melamar(
            nama_pelamar=newLamar['namaPelamar'],
            pesan=newLamar['pesan'],
            kontak_pelamar=newLamar['kontakPelamar'],
            cv_pelamar=newLamar['cvPelamar'],
            id_project=newLamar['idProject'],
            id_user = newLamar['idUser'],
        )

        new_lamar.save()
        return JsonResponse({"instance": "Lamaran berhasil dikirim!"}, status=200)
