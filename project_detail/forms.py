from django import forms
from django.db import models
from buat_project.models import Project

class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['nama_pemilik', 'nama_project', 'kategori', 'bayaran', 'deskripsi', 'estimasi', 'skills', 'kontak_pemilik', 'jumlah_orang']
        labels = {
            'nama_pemilik': 'Nama Pemilik', 
            'nama_project': 'Nama Proyek',
            'kategori': 'Kategori',
            'bayaran': 'Bayaran',
            'deskripsi': 'Deskripsi',
            'estimasi': 'Estimasi Waktu',
            'skills': 'Skill yang dibutuhkan',
            'kontak_pemilik': 'Kontak',
            'jumlah_orang' : 'Jumlah orang yang dibutuhkan'
        }
        widgets = {
            'nama_pemilik': forms.TextInput(attrs={'class': 'short field'}),
            'nama_project': forms.TextInput(attrs={'class': 'short field'}),
            'kategori': forms.TextInput(attrs={'class': 'short field'}),
            'bayaran': forms.TextInput(attrs={'class': 'short field'}),
            'deskripsi': forms.Textarea(attrs={'class': 'message field'}),
            'estimasi': forms.TextInput(attrs={'class': 'short field'}),
            'skills': forms.TextInput(attrs={'class': 'short field'}),
            'kontak_pemilik': forms.Textarea(attrs={'class': 'message field'}),
            'jumlah_orang' : forms.TextInput(attrs={'class': 'short field'}),
        }