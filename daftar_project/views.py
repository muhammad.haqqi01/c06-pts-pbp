import json
from .forms import SuscribeForm
from buat_project.models import Project
from django.core import serializers
from django.http.response import JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from signup.models import User

def index(request):
    form = SuscribeForm(request.POST or None)
    context = {
		'title':'Daftar Proyek',
		'heading':'Daftar Proyek',
        'subscribeForm':form,
    }
    if (form.is_valid() and request.method == 'POST'):
        request.user.is_subscribed = True
        request.user.save()
        return redirect('/')

    return render(request, 'daftar_project/index/index_daftar_project.html', context)

def get_project(request):
    project = Project.objects.all()
    data = serializers.serialize('json', project)
    if(request.user.is_authenticated):
        user_id = request.user.id
        is_admin = request.user.is_admin
        response = {
            'projects':data,
            'user_id':user_id,
            'is_admin':is_admin
            }
    else:
        response = {
            'projects':data,
            }
    return JsonResponse(response)

def my_project(request):
    context = {
		'title':'Proyek Saya',
		'heading':'Proyek Saya',
    }

    if(request.user.is_authenticated):
        return render(request, 'daftar_project/my_project/my_project.html', context)
    else:
        return redirect('/login')

@csrf_exempt
def subscribe(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        email = data["email"]
        # password = data["password"]
        # user = authenticate(email=email, password=password)
        user = User.objects.get(email = email)
        if (user is not None):
            user.is_subscribed = True
            user.save()
            return JsonResponse({"message": "Terima kasih telah berlangganan!"})
        return JsonResponse({"message": "Gagal!"})
