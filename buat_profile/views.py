from django.shortcuts import render, redirect
from django.http.response import HttpResponseRedirect, JsonResponse
from django.http.response import HttpResponse
from signup.models import User
from .forms import UpdateProfil
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, redirect
from django.views.decorators.csrf import csrf_protect, csrf_exempt
import json
#from .form import classForm
# Create your views here.
def index(request):
    if not request.user.is_authenticated:
        return redirect("/login") 
    users = User.objects.all().values()
    response = {'users':users}
    return render(request, 'PageProfil.html', response)
def edit(request, *args, **kwargs):
    if not request.user.is_authenticated:
        return redirect("/login") 
    user_id = request.user.id
    try:
        account = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        return HttpResponse("Something went wrong")
    context = {}
    if request.POST:
        form = UpdateProfil(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/buat_profile')
        else :
            form = UpdateProfil(request.POST, instance= request.user,       
                initial = {
                    "nama" : account.nama,
                    "jenis_kelamin" : account.jenis_kelamin,
                    "institusi" : account.institusi,
                    "email" : account.email,
                    "kontak" : account.kontak
                }   
            )
            context['form'] = form
            validate_email(request)
    else :
        form = UpdateProfil(request.POST, instance= request.user,       
                initial = {
                    "nama" : account.nama,
                    "jenis_kelamin" : account.jenis_kelamin,
                    "institusi" : account.institusi,
                    "email" : account.email,
                    "kontak" : account.kontak
                }   
            ) 
        context['form'] = form
    validate_email(request)
    return render(request, 'FormProfil.html', context)

def validate_email(request):
    email = request.GET.get('email', None)
    data = {
        'is_taken': User.objects.exclude(id = request.user.id).filter(email__iexact=email).exists()
    }
    return JsonResponse(data)
#khusus flutter
@csrf_exempt
def index_flutter(request, id):
    """
    print(request.user)
    if not request.user.is_authenticated:
        return JsonResponse({"data":"unlogin", "status": 200}, status=200)
    user_id = request.user.id
    print(user_id)
    """
    try :
        user = User.objects.get(id = id)
    except:
        return HttpResponse("User ini tidak ada")
    context = {}
    #print(user_id);
    context["data"] = {
        "nama" : user.nama,
        "jenis_kelamin" : user.jenis_kelamin,
        "institusi" : user.institusi,
        "kontak" : user.kontak,
        "email" : user.email,
    }
    print(context);
    return JsonResponse({"data":context, "status": 200}, status=200)
    #response = {'users':users}
    #return render(request, 'PageProfil.html', response)
@csrf_exempt
def edit_flutter(request, id):
    try :
        user = get_object_or_404(User,id=id)
    except:
        print("errorr")
    context = {}
    if request.method == "POST" :
        newData = json.loads(request.body)
        user.nama = newData['nama']
        user.jenis_kelamin = newData['jenis_kelamin']
        user.institusi = newData['institusi']
        user.kontak = newData['kontak']
        user.email = newData['email']
        user.save()
        print("tes111")
        return JsonResponse({"data":"User berhasil diedit", "status": 200}, status=200)
    
    context["data"] = {
        "nama" : user.nama,
        "jenis_kelamin" : user.jenis_kelamin,
        "institusi" : user.institusi,
        "kontak" : user.kontak,
        "email" : user.email,
    }
    print("tes2222")
    return JsonResponse({"data":context, "status": 200}, status=200)
@csrf_exempt   
def validate_email_flutter(request,id) :
    data = {}
    if request.method == "POST" :
        newData = json.loads(request.body)
        email =  newData['email']
        print(email)
        print(id)
        if User.objects.exclude(id = id).filter(email__iexact=email).exists() :
            data["validasi"] = "terpakai"
        else :
            data["validasi"] = "aman"
        return JsonResponse({"data": data,"status":200}, status=200)
    data["validasi"] = "belum tervalidasi"
    return JsonResponse({"data": data, "status":200}, status=200)
    