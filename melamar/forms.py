from django import forms
from django.forms import fields
from melamar.models import Melamar

class MelamarForm(forms.ModelForm):
    class Meta:
        model = Melamar
        fields = ['nama_pelamar', 'pesan', 'kontak_pelamar', 'cv_pelamar']