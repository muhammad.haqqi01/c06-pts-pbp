$("#btncreate").click(function () {
  console.log("Tanyakan Button Clicked")
  let q = $("#q_id").val();
  let csr =$("input[name=csrfmiddlewaretoken").val();
  if(q == "") {
    console.log("Tanyalah dengan benar!")
  } else {
    console.log(q)
    output = ""
    q_data = {question: q, 
      csrfmiddlewaretoken: csr};
    $.ajax({
      url: "create_q/",
      method: "POST",
      data: q_data,
      success:function(data) {
        console.log(data);
        q_list = data.question_data;
        if(data.form_is_valid==true) {
          console.log("Form Submitted Successfully")
          for (i=0; i<q_list.length; i++) {
            output += '<div class="accordion-item"><h2 class="accordion-header" id="heading'
            + q_list[i].id + '"><button id="accord" class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse'
            + q_list[i].id + '" aria-expanded="true" aria-controls="collapse'
            + q_list[i].id + '">' 
            + q_list[i].question + '</button></h2><div id="collapse'
            + q_list[i].id + '" class="accordion-collapse collapse" aria-labelledby="heading'
            + q_list[i].id + '" data-bs-parent="#accordionExample"><div class="accordion-body"><strong>Answer: </strong>'
            + q_list[i].answer + '</div></div></div>'
          }
          $("#accordionExample").html(output);
          $("form")[0].reset();
        }
      },
    });
  }
});