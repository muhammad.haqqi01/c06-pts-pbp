$(document).ready(function () {
    $.ajax({
      url: "/get_project",
      success: function (results) {
        let counter = 0;
        JSON.parse(results.projects).map((result) => {
          if (result.fields.buka && result.fields.acc) {       
            $(".list-project").append(`
              <div class="card-cover" style="margin: 5px 0;">
                  <div class="card">
                  <div class="card-body">
                      <h5 class="card-title">${result.fields.nama_project}</h5>
                      <div class="row">
                        <div class="col border border-3 rounded-start">
                            <p class="">Bayaran</p>
                            <p class="">Rp${result.fields.bayaran}</p>
                        </div>
                        <div class="col border border-3 rounded-end">
                            <p class="">Estimasi waktu</p>
                            <p class="">${result.fields.estimasi}</p>
                        </div>                          
                      </div>
                      <p class="card-text">${result.fields.deskripsi}</p>
                      <a href="/project/detail/${result.pk}" class="btn btn-secondary">More</a>
                  </div>
                  </div>
              </div>`);
            counter++;}
        });
        if (counter == 0){
          $(".list-project").append(`
           <p style="font-weight: normal; font-size: 15px; padding: 10px; color: #5f5d60;">Tidak ada proyek yang tersedia untuk saat ini</p>`)
        };
      },
    });
  });