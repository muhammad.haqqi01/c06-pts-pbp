$(document).on('submit', '#post-form',function(e){
    e.preventDefault();
    links = window.location.href.split('/')
    id = links[links.length-1]
    $.ajax({
        type:'POST',
        url:"{% url 'melamar:add_lamar' %}",
        data:{
            nama_pelamar:$('#nama_pelamar').val(),
            pesan:$('#id_pesan').val(),
            kontak_pelamar:$('#id_kontak_pelamar').val(),
            cv_pelamar:$('#id_cv_pelamar').val(),
            id_project:id,
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
        },
        dataType: 'json',
        success:function(json){
            alert("Lamaran Diterima");
            document.getElementById("post-form").reset();
        }
    });
});
