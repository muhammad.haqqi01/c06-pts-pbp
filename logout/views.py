from django.shortcuts import render
from django.contrib.auth import login, authenticate, logout
from django.shortcuts import render, redirect
from login.forms import UserAuthenticationForm
from django.http.response import Http404, HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import AnonymousUser
# Create your views here.

def logout_view(request):
	logout(request)
	request.session.flush()
	request.user = AnonymousUser
	return redirect("daftar_project:index_daftar_project")