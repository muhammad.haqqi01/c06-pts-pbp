from django.urls import path
from melamar.views import add_lamar, connect_melamar
app_name='melamar'
urlpatterns = [
    path('lamar/<id>', add_lamar, name='add_lamar'),
    path('lamar/<id>/lamar_flutter', connect_melamar, name = 'connect_melamar'),
]