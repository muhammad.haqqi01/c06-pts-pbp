from django.urls import path
from . import views

app_name='buat_project'

urlpatterns = [
    path('', views.buat_project, name='buat_project'),
    path('post_flutter/<id>', views.buat_project_flutter, name='buat_project_flutter')
]
