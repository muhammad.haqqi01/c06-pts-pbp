# C06 PTS PBP

Nama anggota:
- 2006596592	Mikael Alvian Rizky
- 2006596075	Muhammad Haqqi Al Farizi
- 2006596150	Mochammad Agus Yahya
- 2006595791	Dimas Ichsanul Arifin
- 2006596491	Charles Pramudana
- 2006596522	Gitan Sahl Tazakha Wijaya
- 2006595936	Jeremy Reeve Kurniawan


Link herokuapp: https://project-channel.herokuapp.com


Cerita Aplikasi
Pandemi ini telah berdampak pada seluruh kalangan masyarakat, termasuk programmer. Banyak yang kena PHK. Akibatnya, kita membuat web ini dengan tujuan agar programmer dapat mencari nafkah meskipun sudah kena PHK. Web ini berfungsi sebagai penyalur proyek dari orang yang ingin membuat proyek, tetapi tidak punya sumber daya manusia yang memadai kepada programmer yang tidak memiliki pekerjaan.


1. FAQ (form nambahin FAQ (ada di tampilan admin))  (Gitan Sahl Tazakha Wijaya)
    - Form Pertanyaan
    - Daftar FAQ (ajax)
    <br>
2. Daftar Project & Project saya (Searching Project, sekalian ajax) (Dimas Ichsanul Arifin)
    - Lihat list semua proyek
    - Lihat list proyek miliknya
    <br>
3. Profile (Mochammad Agus Yahya)
    - Lihat profil
    - Edit profil
    <br>
4. Melamar (Form melamar (object nyimpen ke array project, di load pake ajax)) (Jeremy Reeve Kurniawan)
    - Nama
    - Pesan
    - kontak yang dapat dihubungi
    - CV
    <br>
5. Halaman Project (Muhammad Haqqi Al Farizi)
    <br>
    Terdapat 2 halaman, yaitu detail project & edit project
    <br>Yang ditampilkan pada detail project :
    - Nama project
    - kategori
    - bayaran
    - deskripsi
    - estimasi waktu
    - skill yang diminta
    - kontak klien
    - jumlah orang yang dibutuhkan
    - Daftar lamaran (Admin/pemilik proyek)
    <br>
    Halaman edit project berfungsi untuk mengedit semua yang ditampilkan di atas kecuali daftar lamaran
    <br><br>
6. Buat Project (Form data project (ajax kirim ke daftar project)) (Mikael Alvian Rizky)
    - Nama project
    - kategori (frontend/backend)
    - bayaran
    - deskripsi
    - estimasi waktu
    - skill yang diminta
    - kontak klien
    - jumlah orang yang dibutuhkan
    <br>
7. Login/Logout (bikin buat user sama admin) + Sign up (Form data pengguna baru) (Charles Pramudana)
    - Login
    - email
    - password
    - Form sign up (ajax):
    - Nama
    - Jenis kelamin
    - Asal institusi
    - Email
    - Password
    - Kontak yang dapat dihubungi 
    <br>
Peran-peran pengguna:
- User nonlogin:
    - Melihat Daftar Proyek
    - Melihat FAQ
    - Membuat akun

- User login:
    - Sama seperti user nonlogin (kecuali membuat akun)
    - Membuat proyek
    - Melamar proyek
    - Mengedit profile
    - Melihat daftar pelamar pada proyek miliknya
    - Mengedit proyek miliknya
    - Menutup proyek miliknya
    
- Admin:
    - Sama seperti user login
    - Menjawab FAQ
    - Melihat daftar pelamar pada semua proyek
    - Mengapprove proyek
    - Menutup semua proyek

