from django.urls import path
from .views import index, edit, validate_email, index_flutter, edit_flutter, validate_email_flutter
from django.conf.urls import url

app_name = 'profile'

urlpatterns = [
    path('', index, name='index'),
    path('edit', edit),
    url('validate_email/', validate_email, name='validate_email'),
    path('<id>', index_flutter),
    path('<id>/edit', edit_flutter),
    path('<id>/validate_email_flutter', validate_email_flutter),
]