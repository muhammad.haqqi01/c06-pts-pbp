from django.urls import path
from .views import get_project, index, my_project, subscribe

app_name = 'daftar_project'

urlpatterns = [
    path('', index, name='index_daftar_project'),
    path('get_project/', get_project, name='get_project_json'),
    path('my_project/', my_project, name='my_project'),
    path('subscribe/', subscribe, name='subscribe')
]