import os
import json
from django.db.models import fields
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, redirect
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.contrib.auth.decorators import login_required
from buat_project.models import Project
from melamar.models import Melamar
from django.http.response import HttpResponse
from django.core import serializers
from project_detail.forms import ProjectForm
from signup.models import User

# Create your views here.
@login_required(login_url='../../../login/')
def index(request, id):
    try:
        context ={}
        data = Project.objects.get(id = id)
        if request.user.is_admin or data.id_pemilik == request.user:
            context["data"] = data
            data = Melamar.objects.filter(id_project = id)
            context["data_lamar"] = data
            return render(request, 'project_detail.html', context)
        elif data.buka and data.acc :
            context["data"] = data
            data = Melamar.objects.filter(id_project = id)
            context["data_lamar"] = data
            return render(request, 'project_detail.html', context)
        else:
            return HttpResponse("Proyek ini tidak dibuka atau tidak ada")
    except:
        return HttpResponse("Proyek ini tidak dibuka atau tidak ada")

@login_required(login_url='../../../login/')
def edit_project(request, id):
    try:
        obj = get_object_or_404(Project, id = id)
    except:
        return HttpResponse("Proyek ini tidak dibuka atau tidak ada")
        
    if obj.id_pemilik == request.user:
        context ={}
        form = ProjectForm(request.POST or None, instance = obj)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect("../"+id)
        context["form"] = form
        return render(request, "project_edit.html", context)
    else:
        return HttpResponse("Anda bukanlah pemilik proyek ini")

@login_required(login_url='../../../login/')
@csrf_exempt
def close_project(request, id):
    try:
        obj = Project.objects.get(id = id)
    except:
        return HttpResponse("Proyek ini tidak pernah ada")

    if request.method =="POST" and request.is_ajax:
        obj.buka = 0
        obj.save()
        return JsonResponse({"instance": "Proyek Ditutup"}, status=200)
    return JsonResponse({"error": "Gagal Ditutup"}, status=400)

@login_required(login_url='../../../login/')
@csrf_exempt
def delete_project(request, id):
    try:
        obj = Project.objects.get(id = id)
    except:
        return HttpResponse("Proyek ini tidak pernah ada")

    if request.method =="POST" and request.is_ajax:
        obj.delete()
        return JsonResponse({"instance": "Proyek Dihapus"}, status=200)
    return JsonResponse({"error": "Delete Ditolak"}, status=400)

@login_required(login_url='../../../login/')
@csrf_exempt
def approve_project(request, id):
    try:
        obj = Project.objects.get(id = id)
    except:
        return HttpResponse("Proyek ini tidak pernah ada")

    is_owner = 0
    if obj.id_pemilik == request.user:
        is_owner = 1
        
    if request.method =="POST" and request.is_ajax:
        obj.acc = 1
        obj.save()
        return JsonResponse({"instance": is_owner}, status=200)
    return JsonResponse({"error": ""}, status=400)
"""
@login_required(login_url='../../../login/')
def download_cv(request, filename):
    try:
        with open(os.path.join('', filename), 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/pdf")
            response['Content-Disposition'] = 'attachment; filename=%s' % filename
            return response
    except:
        return HttpResponse("File ini tidak pernah ada")
"""

def index_flutter(request, id):
    try:
        context ={}
        data = Project.objects.get(id = id)
        """
        if request.user.is_admin or data.id_pemilik == request.user:
            context["data"] = data
            data = Melamar.objects.filter(id_project = id)
            context["data_lamar"] = data
            return JsonResponse({'context': context}, status=200)
        elif data.buka and data.acc :
            context["data"] = data
            data = Melamar.objects.filter(id_project = id)
            context["data_lamar"] = data
            return JsonResponse({'context': context}, status=200)
        """
        context["data"] = {
            "nama_pemilik": data.nama_pemilik,
            "id_pemilik": data.id_pemilik.username,
            "nama_project": data.nama_project,
            "kategori": data.kategori,
            "bayaran": data.bayaran,
            "deskripsi": data.deskripsi,
            "estimasi": data.estimasi,
            "skills": data.skills,
            "kontak_pemilik": data.kontak_pemilik,
            "jumlah_orang": data.jumlah_orang,
            "acc": data.acc,
            "buka": data.buka
        }
        data = Melamar.objects.filter(id_project = id)
        context["data_lamar"] = []
        for x in data:
            context["data_lamar"].append(
                {
                    "nama_pelamar": x.nama_pelamar,
                    "pesan": x.pesan,
                    "kontak_pelamar": x.kontak_pelamar,
                    "cv_pelamar": x.cv_pelamar,
                    "id_project": x.id_project,
                    "id_user": x.id_user,
                }
            )
        print(context)
        return JsonResponse({"data":context, "status": 200}, status=200)
    except Exception as e:
        print(e)
        return JsonResponse({"data":"Proyek ini tidak dibuka atau tidak ada", "status": 404}, status=404)

@csrf_exempt
def edit_project_flutter(request, id):
    #return JsonResponse({"data": "Masuk edit"})
    try:
        data = get_object_or_404(Project, id = id)
    except:
        return JsonResponse({"data":"Proyek ini tidak dibuka atau tidak ada", "status": 404}, status=404)
    
    #if obj.id_pemilik == request.user:
    context ={}
    if request.method == 'POST':
        newData = json.loads(request.body)
        data.nama_pemilik = newData['nama_pemilik']
        data.nama_project = newData['nama_project']
        data.kategori = newData['kategori']
        data.bayaran = int(newData['bayaran'])
        data.deskripsi = newData['deskripsi']
        data.estimasi = newData['estimasi']
        data.skills = newData['skills']
        data.kontak_pemilik = newData['kontak_pemilik']
        data.jumlah_orang = int(newData['jumlah_orang'])
        data.save()

        return JsonResponse({"data":"Proyek berhasil diedit", "status": 200}, status=200)
    #form = ProjectForm(request.POST or None, instance = obj)

    #if form.is_valid():
        #form.save()
        #return JsonResponse({"data":"Proyek berhasil diedit"}, status=200)
    context["data"] = {
        "nama_pemilik": data.nama_pemilik,
        "id_pemilik": data.id_pemilik.username,
        "nama_project": data.nama_project,
        "kategori": data.kategori,
        "bayaran": data.bayaran,
        "deskripsi": data.deskripsi,
        "estimasi": data.estimasi,
        "skills": data.skills,
        "kontak_pemilik": data.kontak_pemilik,
        "jumlah_orang": data.jumlah_orang,
        "acc": data.acc,
        "buka": data.buka
    }
    return JsonResponse({"data":context, "status": 200}, status=200)
    #else:
    #    return JsonResponse({"data":"Proyek ini tidak dibuka atau tidak ada"}, status=404)

@csrf_exempt
def close_project_flutter(request, id):
    try:
        obj = Project.objects.get(id = id)
    except:
        return JsonResponse({"data":"Proyek ini tidak pernah ada", "status": 404}, status=404)

    if request.method =="POST":
        obj.buka = 0
        obj.save()
        return JsonResponse({"instance": "Proyek Ditutup", "status": 200}, status=200)
    return JsonResponse({"error": "Gagal Ditutup", "status": 400}, status=400)

@csrf_exempt
def delete_project_flutter(request, id):
    try:
        obj = Project.objects.get(id = id)
    except:
        return JsonResponse({"data":"Proyek ini tidak pernah ada", "status": 404}, status=404)

    if request.method =="POST":
        obj.delete()
        return JsonResponse({"instance": "Proyek Dihapus", "status": 200}, status=200)
    return JsonResponse({"error": "Delete Ditolak", "status": 400}, status=400)

@csrf_exempt
def approve_project_flutter(request, id):
    try:
        obj = Project.objects.get(id = id)
    except:
        return JsonResponse({"data":"Proyek ini tidak pernah ada", "status": 404}, status=404)

    userData = json.loads(request.body)['username']
    is_owner = 0
    if obj.id_pemilik == userData:
        is_owner = 1
        
    if request.method =="POST":
        obj.acc = 1
        obj.save()
        return JsonResponse({"instance": is_owner, "status": 200}, status=200)
    return JsonResponse({"error": "", "status": 400}, status=400)
