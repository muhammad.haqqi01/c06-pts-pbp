from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from .models import Project
from signup.models import User
from django.contrib.auth.decorators import login_required
import json

@login_required(login_url="/login/")
def buat_project(request):
    if request.method == 'POST':
        nama_pemilik = request.POST.get('nama_pemilik')
        nama_project = request.POST.get('nama_project')
        kategori = request.POST.get('kategori')
        bayaran = request.POST.get('bayaran')
        deskripsi = request.POST.get('deskripsi')
        estimasi = request.POST.get('estimasi')
        skills = request.POST.get('skills')
        kontak_pemilik = request.POST.get('kontak_pemilik')
        jumlah_orang = request.POST.get('jumlah_orang')

        new_project = Project(
            nama_pemilik=nama_pemilik,
            nama_project=nama_project,
            kategori=kategori,
            bayaran=bayaran,
            deskripsi=deskripsi,
            estimasi=estimasi,
            skills=skills,
            kontak_pemilik=kontak_pemilik,
            jumlah_orang=jumlah_orang,
            id_pemilik=User.objects.get(id=request.user.id),
            )

        new_project.save()
        
        return JsonResponse({"instance": "Proyek Dibuat"}, status=200)
    return render(request, 'buat_project.html')

@csrf_exempt
def buat_project_flutter(request, id):
    if request.method == 'POST':
        newProject = json.loads(request.body)

        new_project = Project(
            nama_pemilik=newProject['namaPemilik'],
            nama_project=newProject['namaProject'],
            kategori=newProject['kategori'],
            bayaran=int(newProject['bayaran']),
            deskripsi=newProject['deskripsi'],
            estimasi=newProject['estimasi'],
            skills=newProject['skills'],
            kontak_pemilik=newProject['kontakPemilik'],
            jumlah_orang=int(newProject['jumlahPelamar']),
            id_pemilik=User.objects.get(id=newProject['idPemilik']),
        )

        new_project.save()
        return JsonResponse({"instance": "Proyek Berhasil Dibuat!"}, status=200)
