
from django.shortcuts import render,get_object_or_404
from django.contrib.auth import login, authenticate, logout
from django.shortcuts import render, redirect
from login.forms import UserAuthenticationForm
from django.http.response import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib.auth.models import AnonymousUser
import json
from django.http import JsonResponse
from signup.forms import RegistrationForm
from signup.models import MyAccountManager, User
from django.views.decorators.csrf import csrf_exempt, csrf_protect
# Create your views here.

@csrf_exempt
def login_flutter(request):
	context = {}
	user = request.user
	
	if request.method == "POST":
		data = json.loads(request.body)
		email = data['email']
		password = data['password']
		user = authenticate(request, email=email, password=password)
		if user:
			login(request, user)
			context['login'] = "logged-in"
			context['user'] = {"email": user.email, "username": user.username, 
	"nama": user.nama, "institusi": user.institusi, "jenis_kelamin": user.jenis_kelamin, 
	"kontak": user.kontak, "is_admin": user.is_admin, "is_subscribed": user.is_subscribed, "id" : user.id}
			return JsonResponse({'data': context}, status=200)

	context['login'] = 'unlogin'
	return JsonResponse({'data': context}, status=500)

@csrf_exempt
def signup_flutter(request):
	if request.method == 'POST':
		data = json.loads(request.body)
		email = data['email']
		username = data['username']
		password = data['password']
		nama = data['nama']
		jenis_kelamin = data['jenis_kelamin']
		institusi = data['institusi']
		kontak = data['kontak']
		
		try:
			new_user = User.objects.create_user(email, username, password)
			new_user.nama = nama
			new_user.jenis_kelamin = jenis_kelamin
			new_user.institusi = institusi
			new_user.kontak = kontak
			new_user.save()
			return JsonResponse({"instance": "user Dibuat"}, status=200)
		except:
			return JsonResponse({"instance": "gagal Dibuat"}, status=400)

	return JsonResponse({"instance": "gagal Dibuat"}, status=400)

@csrf_exempt
def logout_flutter(request):
	data = json.loads(request.body)
	if request.user.is_authenticated or data['loggedIn']:
		if request.user.is_authenticated:
			logout(request)
		return JsonResponse({"status" : "loggedout"}, status=200)
	return JsonResponse({"status": "Not yet authenticated"}, status =403)

@csrf_exempt
def get_user(request):
	user = request.user
	if not user.is_authenticated:
		return JsonResponse({"result": "Not yet authenticated!"}, status=403)
	
	user = User.objects.get(username=user)
	return JsonResponse({"data": {"email": user.email, "username": user.username, 
	"nama": user.nama, "institusi": user.institusi, "jenis_kelamin": user.jenis_kelamin, 
	"kontak": user.kontak, "is_admin": user.is_admin, "is_subscribed": user.is_subscribed, "id" : user.id}}, status=200)