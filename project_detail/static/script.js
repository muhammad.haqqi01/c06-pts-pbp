/*
$(window).on('load', function () {
    detectUser();
});
*/
$("body").on('click', '#openModal', function(){
    $(".closingModal").css("display", "flex")
    if($(this).html() == 'Tolak'){
        $("#modalMessage").text("Apakah anda ingin menolak proyek ini?")
        $("#closeProjectButton").css("display", "none")
        $("#deleteButton").text("Tolak")
    }
    else {
        $("#modalMessage").text("Apakah anda ingin menutup proyek ini?")
        $("#deleteButton").css("display", "none")
        $("#closeProjectButton").text("Tutup")
    }
});

$("body").on('click', '#closeModal', function(){
    $(".closingModal").css("display", "none")
});

$("body").on('click', '#closeProjectButton', function(){
    links = window.location.href.split('/')
    id = links[links.length-1]
    $.ajax({
        url: "closeproject/" + id,
        type: 'POST',
        success: function(response){
            alert("Close Success")
            $(".closingModal").css("display", "none")
            $(".bottom").css("display", "none")
            $("#buttons").css("display", "none")
        },
        error: function(response){
            alert("Close Failed")
        }
    });
});

$("body").on('click', '#deleteButton', function(){
    links = window.location.href.split('/')
    id = links[links.length-1]
    $.ajax({
        url: "deleteproject/" + id,
        type: 'POST',
        success: function(response){
            alert("Delete Success")
            window.location.replace(links[0] + '/' + links[1] + '/' + links[2]);
        },
        error: function(response){
            alert("Delete Failed")
        }
    });
});

$("body").on('click', '#approveButton', function(){
    links = window.location.href.split('/')
    id = links[links.length-1]

    $.ajax({
        url: "approveproject/" + id,
        type: 'POST',
        dataType: 'json',
        success: function(response){
            alert("You have approved this project")   
            $(".bottom").css("display", "flex")
            if (response.instance) {
                $("#buttons").html("<a class='button minus' id='openModal'>Tutup proyek</a><a class='button secondary' href='" + window.location.href + "/edit'>Edit proyek</a>")
            }
            else{
                $("#buttons").html("<a class='button primary' id='lamarButton' href='../../add_lamar/lamar/" + id + "'>Lamar</a><a class='button minus' id='openModal'>Tutup proyek</a>")
            }
        },
        error: function(response){
            alert('Approve Failed')
        }
    });
});    
