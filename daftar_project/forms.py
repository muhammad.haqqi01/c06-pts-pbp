# import form class from django
from django import forms
from signup.models import User

class SuscribeForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('is_subscribed',)
        labels = {
            'is_subscribed': '', 
        }
        widgets = {
            'is_subscribed': forms.Select(attrs={'style': 'display:none;'}),
        }