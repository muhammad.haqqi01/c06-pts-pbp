from django.urls import path
from .views import delete_project, index, edit_project, approve_project, close_project, index_flutter, edit_project_flutter, close_project_flutter, delete_project_flutter, approve_project_flutter

urlpatterns = [
    path('detail/<id>', index),
    path('detail/<id>/edit', edit_project),
    path('detail/closeproject/<id>', close_project),
    path('detail/deleteproject/<id>', delete_project),
    path('detail/approveproject/<id>', approve_project),
    #path('detail/download/<filename>', download_cv),

    path('detail/<id>/flutter', index_flutter),
    path('detail/<id>/edit/flutter', edit_project_flutter),
    path('detail/closeproject/<id>/flutter', close_project_flutter),
    path('detail/deleteproject/<id>/flutter', delete_project_flutter),
    path('detail/approveproject/<id>/flutter', approve_project_flutter),
]