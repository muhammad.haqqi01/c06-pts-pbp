"""project_channel URL Configuration

The urlpatterns list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import project_detail.urls as project_detail
import signup.urls as signup
import login.urls as login
import daftar_project.urls as daftar_project
import buat_project.urls as buat_project
import faq.urls as faq
import buat_profile.urls as buat_profile
import melamar.urls as melamar
import logout.urls as logout
import auth_flutter.urls as auth_flutter

urlpatterns = [
    path('add_lamar/', include(melamar)),
    path('auth/', include(auth_flutter)),
    path('admin/', admin.site.urls),
    path('project/', include(project_detail)),
    path('signup/', include(signup)),
    path('login/', include(login)),
    path('logout/', include(logout)),
    path('buat_profile/', include(buat_profile)),
    path('buat_project/', include(buat_project)),
    path('faq/', include(faq)),
    path('', include(daftar_project)),
]
