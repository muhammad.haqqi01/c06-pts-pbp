from django.http import response, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from .models import Faq
from .forms import FaqForm
import json

# Create your views here.
def index(request):
    form = FaqForm()
    faq = Faq.objects.all()
    response = {'title' : 'Frequently Asked Question',
                'faq' : faq,
                'form' : form}
    if request.user.is_authenticated:
        return render(request, 'index_faq_logged.html', response)
    else:
        return render(request, "index_faq.html", response)

# @csrf_exempt
def create_q(request):
    data = dict()
    if request.method=="POST":
        form = FaqForm(request.POST)
        if form.is_valid():
            data['form_is_valid'] = True
            question = request.POST['question']
            faq = Faq(question=question)
            faq.save()
            question = Faq.objects.values()
            data['question_data'] = list(question)
            return JsonResponse(data)
        else:
            data['form_is_valid'] = False
            return JsonResponse(data)

@csrf_exempt
def mobileJson(request):
    if request.method == "POST":
        data = json.loads(request.body)
        qna = Faq(question = data['question'], answer="")
        qna.save()
    faq_obj = Faq.objects.all()
    faq_data = json.loads(serializers.serialize('json', faq_obj))
    return JsonResponse(faq_data, safe=False)
