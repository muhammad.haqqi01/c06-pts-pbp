from django import forms
from signup.models import User

class UpdateProfil(forms.ModelForm):
    class Meta:
        model = User
        fields = ['nama','jenis_kelamin','institusi','email','kontak']

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        try:
            account = User.objects.exclude(pk=self.instance.pk).get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError("Email is already in use")
    def save(self, commit=True):
        account = super(UpdateProfil, self).save(commit=False)
        account.email = self.cleaned_data['email']
        if commit:
            account.save()
        return account
    